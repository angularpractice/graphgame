import { Component, OnInit, ViewChild } from '@angular/core';
import { DxFormComponent } from 'devextreme-angular/ui/form';
import { confirm } from 'devextreme/ui/dialog';
import { v4 as uuid } from 'uuid';
import notify from 'devextreme/ui/notify';
import { DxSelectBoxComponent } from 'devextreme-angular';

class Player {
  id: string;
  name: string;
  color: string;

  constructor(
    name: string,
    color: string
  ) {
    this.id = uuid();
    this.name = name;
    this.color = color;
  }
}

class TurnBlock {
  player: Player;

  constructor(player?: Player) {
    this.player = player ? player : null;
  }
}

class TurnForm {
  currentPlayer: Player;
  selectedThrow: number;

  constructor(
    currentPlayer: Player,
    selectedThrow: number,
  ) {
    this.currentPlayer = currentPlayer;
    this.selectedThrow = selectedThrow;
  }
}

class GameForm {
  totalBlocks: number;
  minThrowNumber: number;
  maxThrowNumber: number;
  players: Player[];

  constructor(totalBlocks: number, minThrowNumber: number, maxThrowNumber: number, players: Player[]) {
    this.totalBlocks = totalBlocks;
    this.minThrowNumber = minThrowNumber;
    this.maxThrowNumber = maxThrowNumber;
    this.players = players;
  }
}

enum ModeEnum {
  configurating,
  playing
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  FIELD_REQUIRED_MESSAGE = 'El campo es requerido';
  BUTTON_WIDTH = '3em';

  modeEnum = ModeEnum;
  currentMode: ModeEnum = ModeEnum.configurating;
  gameForm: GameForm = null;
  turnForm: TurnForm = null;
  allTurnBlocks: TurnBlock[] = [];
  availableTurnThrowList: number[] = [];
  startGameButtonOptions: any = {};
  resetGameFormButtonOptions: any = {};
  resetGameButtonOptions: any = {};
  returnToConfigButtonOptions: any = {};
  blankTurnBlocksNumber: number = null;
  defaultPlayers: Player[] = [new Player('Pepito Perez 1', '#e63946'), new Player('Pepito Perez 2', '#023e8a')];

  winningPath: number[] = [];
  winningPathFound = false;

  @ViewChild('gameFormReference') gameFormReference: DxFormComponent;
  @ViewChild('selectedThrowDxSelectBox') selectedThrowDxSelectBox: DxSelectBoxComponent;

  constructor() {

  }

  ngOnInit(): void {
    // Setear opciones botones del toolbar
    this.setToolBarButtonOptions();
    // Setear config inicial
    this.setInitialGame();
  }

  setToolBarButtonOptions = () => {
    // Opciones botón comenzar juego
    this.startGameButtonOptions = {
      icon: 'todo',
      hint: 'Comenzar juego',
      type: 'normal',
      stylingMode: 'outlined',
      width: this.BUTTON_WIDTH,
      onClick: () => {
        if (!this.gameFormReference.instance.validate().isValid) {
          return;
        }
        if(this.gameForm.players.length < 2){
          return notify('🤨 Por favor añadir al menos 2 jugadores', 'info', 2000);
        }
        this.onStartGame();
      }
    };
    // Opciones botón resetear formulario del juego
    this.resetGameFormButtonOptions = {
      icon: 'refresh',
      hint: 'Resetear formulario',
      type: 'normal',
      stylingMode: 'outlined',
      width: this.BUTTON_WIDTH,
      onClick: () => this.onResetForm()
    };
    // Opciones botón restear juego
    this.resetGameButtonOptions = {
      icon: 'refresh',
      hint: 'Resetear juego',
      type: 'normal',
      stylingMode: 'outlined',
      width: this.BUTTON_WIDTH,
      onClick: () => this.onConfirmResetCurrentGame()
    };
    // Opciones botón regresar a configuración del juego
    this.returnToConfigButtonOptions = {
      icon: 'revert',
      hint: 'Regresar a configuración',
      type: 'normal',
      stylingMode: 'outlined',
      width: this.BUTTON_WIDTH,
      onClick: () => this.onConfirmReturnToConfig()
    };
  }

  /**
   * Encargado de setear el config inicial
   */
  setInitialGame = () => {
    // Setear juego
    this.gameForm = new GameForm(
      110,
      1,
      12,
      [...this.defaultPlayers]
    );
  }

  /**
   * Encargado de resetear el formulario
   */
  onResetForm = async () => {
    if (await this.awaitConfirm('Esta seguro de querer limpiar el formulario?')) {
      this.gameForm.maxThrowNumber = null;
      this.gameForm.minThrowNumber = null;
      this.gameForm.totalBlocks = null;
      this.gameForm.players = this.defaultPlayers;
    }
  }

  /**
   * Encargado de crear el juego
   */
  onStartGame = () => {
    this.setAvailableTurnThrowList();
    this.setInitialPlayerTurn();
    this.setAllTurnBlocks();
    this.setWinningPaths();
    this.currentMode = ModeEnum.playing;
  }

  onConfirmResetCurrentGame = async () => {
    if (await this.awaitConfirm('Esta seguro de querer reiniciar la partida?')) {
      this.onResetCurrentGame();
    }
  }

  /**
   * Encargado de resetear el juego actual
   */
  onResetCurrentGame = () => {
    this.allTurnBlocks = this.allTurnBlocks.map((turnBlock: TurnBlock) => {
      turnBlock.player = null;
      return turnBlock;
    });
    this.winningPath = [];
    this.setWinningPaths();
  }

  onConfirmReturnToConfig = async () => {
    if (await this.awaitConfirm('Si se regresa se perdera la partida, esta seguro de querer continuar?')) {
      this.onReturnToConfig();
    }
  }

  /**
   * Encargado de regresar a modo config
   */
  onReturnToConfig = () => {
    this.winningPath = [];
    this.currentMode = ModeEnum.configurating;
  }

  /**
   * Setea listado de tiros disponibles por turno
   */
  setAvailableTurnThrowList = () => {
    this.availableTurnThrowList = [];
    for (let i = this.gameForm.minThrowNumber; i <= this.gameForm.maxThrowNumber; i++) {
      this.availableTurnThrowList.push(i);
    }
  }

  /**
   * Setea turno del jugador inicial
   */
  setInitialPlayerTurn = () => {
    const firstPlayer = this.getRandomPlayer();
    this.turnForm = new TurnForm(firstPlayer, this.gameForm.minThrowNumber);
    setTimeout(() => {
      this.selectedThrowDxSelectBox.instance.open();
    }, 10);
  }

  /**
   * Setea listado de tiros disponibles
   */
  setAllTurnBlocks = () => {
    this.allTurnBlocks = [];
    for (let i = 0; i < this.gameForm.totalBlocks; i++) {
      this.allTurnBlocks.push(new TurnBlock());
    }
  }

  /**
   * Dialogo confirmar generico
   */
  async awaitConfirm(message: string): Promise<boolean> {
    const c = await confirm(message, 'Confirmar!');
    return c;
  }

  /**
   * Valida numero minimo de tiros
   */
  validateMinThrowNumber = (e: { value: number }): boolean => e.value < this.gameForm.maxThrowNumber ? true : false;

  /**
   * Valida numero maximo de tiros
   */
  validateMaxThrowNumber = (e: { value: number }): boolean => e.value > this.gameForm.minThrowNumber ? true : false;

  /**
   * Al añadir jugador en grid de jugadores
   */
  // tslint:disable-next-line: max-line-length
  onPlayerInitNewRow = (e: { data: Player }) => e.data = new Player(`Pepito Perez ${this.gameForm.players.length + 1}`, this.getRandomColor());

  /**
   * Obtiene un color aleatorio
   */
  getRandomColor = () => {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  /**
   * Obtiene un jugador aleatorio
   */
  getRandomPlayer = (): Player => {
    if (!this.gameForm.players || this.gameForm.players.length === 0) {
      console.error('No se obtuvo listado de jugadores');
      return null;
    }
    return this.gameForm.players[Math.floor(Math.random() * this.gameForm.players.length)];
  }

  /**
   * Encargado de realizar el tiro
   */
  onLaunchThrow = () => {
    // Determinar si se puede seguir jugando
    const availableTurnBlocks = this.allTurnBlocks.filter((turnBlock: TurnBlock) => !turnBlock.player);
    if (availableTurnBlocks.length > this.turnForm.selectedThrow) {
      // Si se puede seguir jugando actualizar todos los tiros con los consumidos
      this.onUpdateAllTurnBlocks(availableTurnBlocks);
    } else {
      // En caso de no poder seguir jugando determinar ganador o perdedor
      this.onDetermineWinnerOrLooser(availableTurnBlocks);
    }
  }

  /**
   * Encargado de actualizar todos los turnos
   */
  onUpdateAllTurnBlocks = (availableTurnBlocks: TurnBlock[]) => {
    if (!this.allTurnBlocks[0].player) {
      // En caso de ser el primer jugador actualizar los turnos blancos obtenidos
      this.allTurnBlocks = this.getUpdatedTurnBlocksFromSelectedThrows(availableTurnBlocks);
    } else {
      // En caso de no ser el primer jugador, combinar los turnos ya consumidos con la actualización de los turnos blancos ya obtenidos
      const previouslySelectedTurnBlocks = this.allTurnBlocks.filter((turnBlock: TurnBlock) => turnBlock.player);
      const updatedBlankTurnBlocks = this.getUpdatedTurnBlocksFromSelectedThrows(availableTurnBlocks);
      this.allTurnBlocks = [...previouslySelectedTurnBlocks, ...updatedBlankTurnBlocks];
    }
    // Setear siguiente turno
    this.setNextTurn();
  }

  /**
   * Encargado de determinar el perdedor o ganador de la partida
   */
  onDetermineWinnerOrLooser = async (availableTurnBlocks: TurnBlock[]) => {
    if (this.turnForm.selectedThrow === availableTurnBlocks.length) {
      this.allTurnBlocks = this.allTurnBlocks.map((turnBlock) => {
        if (turnBlock.player) {
          return turnBlock;
        }
        turnBlock.player = this.turnForm.currentPlayer;
        return turnBlock;
      })
      notify('🙂 GANASTE!!!!', 'success', 2000);
      if (await this.awaitConfirm(`🙂 GANASTE ${this.turnForm.currentPlayer.name}!!! Desean repetir este mismo juego?`)) {
        return this.onResetCurrentGame();
      }
      return this.onReturnToConfig();
    } else {
      // Determinar si hubo en definitiva un ganador
      const usedTurnBlocks = this.allTurnBlocks.filter(turnBlock => turnBlock.player);
      if (usedTurnBlocks && usedTurnBlocks.length > 0) {
        // En caso de haber alguien mas cercano a la meta
        if (await this.awaitConfirm(`🙂 GANASTE ${usedTurnBlocks[usedTurnBlocks.length - 1].player.name} al ser el más cercano!!! Desean repetir este mismo juego?`)) {
          return this.onResetCurrentGame();
        }
        return this.onReturnToConfig();
      } else {
        // En caso de no haber nadie cercano a la meta
        notify('🤕 PERDIERON!!!', 'error', 2000);
        if (await this.awaitConfirm(`🤕 PERDIERON!!! Desean repetir este mismo juego?`)) {
          return this.onResetCurrentGame();
        }
        return this.onReturnToConfig();
      }
    }
  }

  /**
   * Obtiene los turnos actualizados a partir de los disponibles
   */
  getUpdatedTurnBlocksFromSelectedThrows = (availableTurnBlocks: TurnBlock[]): TurnBlock[] => {
    const updatedTurnBlocks = availableTurnBlocks.map((turnBlock: TurnBlock, i) => {
      if (i + 1 > this.turnForm.selectedThrow) {
        return turnBlock;
      }
      turnBlock.player = { ...this.turnForm.currentPlayer };
      return turnBlock;
    });
    return updatedTurnBlocks;
  }

  /**
   * Setea el siguiente turno
   */
  setNextTurn = () => {
    this.winningPath = [];
    this.setWinningPaths();
    this.turnForm.selectedThrow = this.gameForm.minThrowNumber;
    const currentPlayerIndex = this.gameForm.players.findIndex((player: Player) => player.id === this.turnForm.currentPlayer.id);
    this.selectedThrowDxSelectBox.instance.open();
    if (currentPlayerIndex !== this.gameForm.players.length - 1) {
      return this.turnForm.currentPlayer = { ...this.gameForm.players[currentPlayerIndex + 1] };
    }
    return this.turnForm.currentPlayer = { ...this.gameForm.players[0] };
  }

  /**
   * Encargado de definir el camino de la victoria
   */
  setWinningPaths = () => {
    this.winningPathFound = false;
    // Obtener bloques vacios
    const blankTurnBlocks = this.allTurnBlocks.filter((turnBlock: TurnBlock) => !turnBlock.player);
    this.blankTurnBlocksNumber = blankTurnBlocks.length;
    // Determinar camino de victoria
    this.winningPath = this.calculateWinningPath(this.blankTurnBlocksNumber);
  }

  /**
   * Encargado de obtener los pasos para ganar
   */
  calculateWinningPath = (availableBlockNumber): number[] => {
    const numberRange = [...this.availableTurnThrowList];
    let notPossibleToWin = false;
    const calculatedPath: number[] = [];
    let calculatedPathTotal: number = this.getTotalFromNumberArray(calculatedPath);
    const minThrowNumber = this.gameForm.minThrowNumber;
    const maxThrowNumber = this.gameForm.maxThrowNumber;
    while (calculatedPathTotal <= availableBlockNumber || notPossibleToWin) {
      // Determinar que si sea posible ganar
      let residual = availableBlockNumber - calculatedPathTotal;
      if (minThrowNumber > availableBlockNumber || minThrowNumber > residual) {
        notPossibleToWin = true;
        break;
      }

      // Verificar el numero que se pueda añadir
      if (maxThrowNumber <= residual) {
        // En caso de poder añadir el numero mayor de tiro
        calculatedPath.push(maxThrowNumber);
        calculatedPathTotal = this.getTotalFromNumberArray(calculatedPath);
      }
      else {
        // En caso de no poder añadir el numero mayor de tiro, encontrar proximo numero mayor que se pueda usar
        numberRange.reverse().forEach(num => {
          residual = availableBlockNumber - calculatedPathTotal;
          if (residual >= num && residual > 0) {
            calculatedPath.push(num);
            calculatedPathTotal = this.getTotalFromNumberArray(calculatedPath);
          }
        });
      }
    }
    return calculatedPath;
  }

  /**
   * Obtiene un total a partir de un listado de numeros
   */
  getTotalFromNumberArray = (numberArray: number[]): number => {
    if (!numberArray || numberArray.length === 0) {
      return 0;
    }
    return numberArray.reduce((total, num) => total + num);
  }
}
